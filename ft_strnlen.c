/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnlen.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 12:42:47 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:16:41 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns the length of the character string 's', but maximum 'len'.
*/

size_t	ft_strnlen(const char *s, size_t maxlen)
{
	size_t	i;

	if (s == NULL)
		return (0);
	i = 0;
	while (s[i])
	{
		if (i >= maxlen)
			return (maxlen);
		i++;
	}
	return (i);
}
