/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_conversion_get.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/25 11:04:32 by adubois           #+#    #+#             */
/*   Updated: 2016/04/27 17:02:23 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Each function grabs a different part of the conversion specification
** and stores it in the given t_conversion data structure.
*/

int			ft_printf_conversion_get_flags(t_conversion *conv, t_result *result)
{
	char	*flags;
	int		pos;

	flags = FLAGS;
	while (-1 != (pos = ft_strchrpos(flags, *(result->format))))
	{
		conv->flags |= (1 << (7 - pos));
		result->format++;
	}
	return (0);
}

int			ft_printf_conversion_get_width(t_conversion *conv, t_result *result)
{
	int		nb;

	if (*(result->format) == '*')
	{
		conv->width = -1;
		result->format++;
		if (-1 == (conv->width_arg =
					ft_printf_conversion_get_arg_index(result)))
			return (-1);
		return (0);
	}
	conv->width = ft_atoi(result->format);
	if (conv->width < 0)
	{
		conv->flags |= 1 << (7 - ft_strchrpos(FLAGS, '-'));
		conv->width *= -1;
	}
	nb = conv->width;
	while (nb > 0)
	{
		nb /= 10;
		result->format++;
	}
	return (conv->width);
}

int			ft_printf_conversion_get_precision(t_conversion *conv,
												t_result *result)
{
	int		nb;

	if (*(result->format) != '.')
		return (0);
	result->format++;
	if (*(result->format) == '*')
	{
		conv->precision = -1;
		result->format++;
		if (-1 == (conv->precision_arg =
					ft_printf_conversion_get_arg_index(result)))
			return (-1);
		return (0);
	}
	if (0 > (conv->precision = ft_atoi(result->format)))
		return (-1);
	nb = conv->precision;
	if (nb == 0 && ft_isdigit(*result->format))
		result->format++;
	while (nb > 0)
	{
		nb /= 10;
		result->format++;
	}
	return (conv->precision);
}

int			ft_printf_conversion_get_modifier(t_conversion *conv,
												t_result *result)
{
	char	*modifiers;

	modifiers = MODIFIERS;
	if (-1 != ft_strchrpos(modifiers, *(result->format)))
	{
		conv->modifier = *(result->format);
		result->format++;
		if (conv->modifier == 'h' && *(result->format) == 'h')
		{
			conv->modifier = 'H';
			result->format++;
		}
		else if (conv->modifier == 'l' && *(result->format) == 'l')
		{
			conv->modifier = 'L';
			result->format++;
		}
	}
	return (0);
}

int			ft_printf_conversion_get_specifier(t_conversion *conv,
												t_result *result)
{
	char	*specifiers;

	specifiers = SPECIFIERS;
	conv->specifier = ft_tolower(*(result->format));
	if ((conv->uppercase = ft_isupper(*(result->format))) &&
		conv->specifier != 'x')
		conv->modifier = 'l';
	if (-1 == ft_strchrpos(specifiers, conv->specifier))
	{
		conv->specifier = 'c';
		conv->arg_index = -1;
	}
	if (conv->uppercase && (conv->specifier == 'p' || conv->specifier == 'i' ||
							conv->specifier == 'n'))
		return (-1);
	result->format++;
	return (0);
}
