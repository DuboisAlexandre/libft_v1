/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_format_conversion.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/31 19:45:11 by adubois           #+#    #+#             */
/*   Updated: 2016/04/27 16:59:06 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The main format function calls each format function one by one when needed.
** At the end of the process the string will be filly formated.
** (padding functions are int the 'format_conversion_padding.c' file)
*/

void			ft_printf_format_conversion(t_u8_string *mbs,
											t_conversion *conv)
{
	if (conv->specifier == 'o' || conv->specifier == 'x' ||
		conv->specifier == 'd' || conv->specifier == 'i' ||
		conv->specifier == 'u' || conv->specifier == 'p')
		ft_printf_format_conversion_precision(mbs, conv);
	if (conv->flags & (1 << (7 - ft_strchrpos(FLAGS, '0'))))
		ft_printf_format_conversion_padding(mbs, conv);
	if (conv->specifier == 'o' || conv->specifier == 'x' ||
		conv->specifier == 'p')
		ft_printf_format_conversion_alternate(mbs, conv);
	if (conv->specifier == 'd' || conv->specifier == 'i')
		ft_printf_format_conversion_sign(mbs, conv);
	ft_printf_format_conversion_padding(mbs, conv);
}

void			ft_printf_format_conversion_precision(t_u8_string *mbs,
											t_conversion *conv)
{
	char	*padding;
	char	*tmp;
	int		size;
	int		negative;

	negative = (mbs->str[0] == '-');
	if (conv->precision == 0 &&
		mbs->str[0] == '0' && mbs->size == 1)
	{
		mbs->str[0] = '\0';
		mbs->size = 0;
	}
	if (conv->precision <= mbs->size - negative)
		return ;
	if (negative)
		mbs->str[0] = '0';
	size = conv->precision - mbs->size + negative;
	padding = ft_printf_get_padding(size, '0');
	padding[0] = (negative) ? '-' : '0';
	tmp = ft_strjoin(padding, mbs->str);
	free(mbs->str);
	free(padding);
	mbs->str = tmp;
	mbs->size += size;
}

void			ft_printf_format_conversion_alternate(t_u8_string *mbs,
											t_conversion *conv)
{
	char	*tmp;

	if (conv->flags & (1 << (7 - ft_strchrpos(FLAGS, '#'))))
	{
		if (conv->specifier == 'o' && mbs->str[0] != '0')
		{
			tmp = ft_strjoin("0", mbs->str);
			free(mbs->str);
			mbs->str = tmp;
			mbs->size++;
		}
		else if ((conv->specifier == 'x' && mbs->str[0] != '\0' &&
					ft_strcmp(mbs->str, "0")) || conv->specifier == 'p')
		{
			if (conv->uppercase)
				tmp = ft_strjoin("0X", mbs->str);
			else
				tmp = ft_strjoin("0x", mbs->str);
			free(mbs->str);
			mbs->str = tmp;
			mbs->size += 2;
		}
	}
}

void			ft_printf_format_conversion_sign(t_u8_string *mbs,
													t_conversion *conv)
{
	char	*tmp;

	if (conv->flags & (1 << (7 - ft_strchrpos(FLAGS, '+'))))
	{
		if (ft_isdigit(mbs->str[0]))
		{
			tmp = ft_strjoin("+", mbs->str);
			free(mbs->str);
			mbs->str = tmp;
			mbs->size++;
		}
	}
	else if (conv->flags & (1 << (7 - ft_strchrpos(FLAGS, ' '))))
	{
		if (ft_isdigit(mbs->str[0]))
		{
			tmp = ft_strjoin(" ", mbs->str);
			free(mbs->str);
			mbs->str = tmp;
			mbs->size++;
		}
	}
}
