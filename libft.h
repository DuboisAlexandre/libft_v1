/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 16:31:23 by adubois           #+#    #+#             */
/*   Updated: 2016/04/29 19:14:14 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# include <stdlib.h>
# include <unistd.h>
# include <string.h>
# include <stdarg.h>
# include <wchar.h>

# define IS_BIG_ENDIAN (*(unsigned short int *)"\0\xFF" < 0x100)
# define GNL_BUFF_SIZE 1024
# define DEFAULT_VECTOR_SIZE 16
# define DEFAULT_PRECISION 6
# define ERROR_FORMAT "Format error"
# define ERROR_CONVERSION "Conversion error"
# define SPECIFIERS "spdiouxcn%"
# define MODIFIERS "hljz"
# define FLAGS "#0- +"

/*
**	Linked list template
*/

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

/*
** Stack template
*/

typedef struct		s_stack
{
	t_list			*top;
	int				size;
}					t_stack;

/*
** Queue template
*/

typedef struct		s_queue
{
	t_list			*top;
	t_list			*bottom;
	int				size;
}					t_queue;

/*
** GNL buffer
*/

typedef struct		s_gnl_file
{
	int					fd;
	int					start;
	int					status;
	char				buffer[GNL_BUFF_SIZE + 1];
	struct s_gnl_file	*next;
}					t_gnl_file;

/*
** Others
*/

typedef struct		s_vector
{
	int		index;
	int		size;
	void	**cells;
}					t_vector;

typedef struct		s_u8_string
{
	int		size;
	char	*str;
}					t_u8_string;

typedef struct		s_conversion
{
	unsigned char	flags;
	char			modifier;
	int				arg_index;
	int				width;
	int				width_arg;
	int				precision;
	int				precision_arg;
	int				uppercase;
	char			specifier;
}					t_conversion;

typedef struct		s_result
{
	int			size;
	int			manual_args;
	va_list		ap;
	va_list		ap_bkp;
	char		*format;
	char		*format_ptr;
	t_vector	*strings;
	t_vector	*convs;
	int			(*fn[10])(struct s_result *result, t_conversion *conv,
							char **str);
}					t_result;

/*
** Memory management
*/

void				*ft_memalloc(size_t size);
void				ft_memdel(void **ap);
void				*ft_memset(void *b, int c, size_t len);
void				ft_bzero(void *s, size_t n);
void				*ft_memcpy(void *dst, const void *src, size_t n);
void				*ft_memccpy(void *dst, const void *src,
								int c, size_t n);
void				*ft_memmove(void *dst, const void *src, size_t len);
void				*ft_memchr(const void *s, int c, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);

/*
** Output
*/

void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char const *s, int fd);
void				ft_putendl_fd(char const *s, int fd);
void				ft_putnbr_fd(int n, int fd);
void				ft_putchar(char c);
void				ft_putstr(char const *s);
void				ft_putendl(char const *s);
void				ft_putnbr(int n);
void				ft_print_memory(const void *addr, unsigned int size);

/*
** File management
*/

int					ft_get_next_line(int const fd, char **line);

/*
** Char control
*/

int					ft_isupper(int c);
int					ft_islower(int c);
int					ft_isalpha(int c);
int					ft_isspace(int c);
int					ft_isdigit(int c);
int					ft_isalnum(int c);
int					ft_isascii(int c);
int					ft_isprint(int c);
int					ft_toupper(int c);
int					ft_tolower(int c);

/*
** String control
*/

size_t				ft_strlen(const char *s);
size_t				ft_strnlen(const char *s, size_t maxlen);
char				*ft_strchr(const char *s, int c);
int					ft_strchrpos(const char *s, char c);
char				*ft_strrchr(const char *s, int c);
int					ft_strrchrpos(const char *str, char chr);
char				*ft_strstr(const char *s1, const char *s2);
char				*ft_strnstr(const char *s1, const char *s2, size_t n);
char				*ft_strcasechr(const char *s, int c);
char				*ft_strcasestr(const char *s1, const char *s2);
int					ft_strchr_count(char *str, int c);
int					ft_strstr_count(char *str, const char *needle);
int					ft_strcasechr_count(char *str, int c);
int					ft_strcasestr_count(char *str, const char *needle);
int					ft_strcmp(const char *s1, const char *s2);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
int					ft_strequ(char const *s1, char const *s2);
int					ft_strnequ(char const *s1, char const *s2, size_t n);

/*
** String management
*/

char				*ft_strnew(size_t size);
void				ft_strdel(char **as);
void				ft_strclr(char *s);
char				*ft_strtoupper(char *str);
char				*ft_strtolower(char *str);
char				*ft_strdup(const char *s1);
char				*ft_strcpy(char *dst, const char *src);
char				*ft_strncpy(char *dst, const char *src, size_t n);
char				*ft_strcat(char *s1, const char *s2);
char				*ft_strncat(char *s1, const char *s2, size_t n);
size_t				ft_strlcat(char *dst, const char *src, size_t size);
char				*ft_strrev(char *str);
char				*ft_strrtrim(char *str);
char				*ft_strltrim(char const *str);
char				*ft_strtrim(char const *s);
void				ft_striter(char *s, void (*f)(char *));
void				ft_striteri(char *s, void (*f)(unsigned int, char *));
char				*ft_strmap(char const *s, char (*f)(char));
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
char				*ft_strsub(char const *s, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);
char				**ft_strsplit(char const *s, char c);
char				*ft_strnreplace(char *haystack, char *needle,
									char *replace, int n);
char				*ft_strreplace(char *haystack, char *needle, char *replace);
int					ft_strarraycount(char **array);
char				**ft_strarraysort(char **tab,
										int (*cmp)(const char *, const char *));
t_list				*ft_strsplittolst(char const *s, char c);

/*
** Wide chars
*/

int					ft_wcslen(wchar_t *wcs);
int					ft_wcstombs(wchar_t *tab, char **str, int size);
int					ft_wctomb(wchar_t chr, char *str);

/*
**	Conversion
*/

int					ft_atoi(const char *str);
char				*ft_itoa(int n);
char				*ft_itoa_base(long int n, int base);
char				*ft_utoa_base(unsigned long int n, int base);

/*
**	Mathematics
*/

int					ft_abs(int n);
int					ft_min(int a, int b);
int					ft_max(int a, int b);

/*
**	Linked list control
*/

int					ft_lstcount(t_list *lst);
void				ft_lstprintstr(t_list *list);

/*
**	Linked list management
*/

t_list				*ft_lstnew(void const *content, size_t content_size);
int					ft_lstnodestrcmp(const t_list *node1, const t_list *node2);
void				ft_lstnodefree(void *content, size_t size);
void				ft_lstadd(t_list **alst, t_list *new);
void				ft_lstpushfront(t_list **alst, t_list *new);
void				ft_lstpushback(t_list **alst, t_list *new);
void				ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
void				ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void				ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list				*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));
void				ft_lstsort(t_list **list, int (*cmp)(const t_list *,
															const t_list *));

/*
** Stack control
*/

int					ft_stackcount(t_stack *stack);

/*
** Stack management
*/

t_stack				*ft_stackcreate(void);
void				ft_stackdelete(t_stack **stack_ptr);
int					ft_stackpush(t_stack *stack, t_list *node);
t_list				*ft_stackpop(t_stack *stack);

/*
** Queue control
*/

int					ft_queuecount(t_queue *queue);

/*
** Queue management
*/

t_queue				*ft_queuecreate(void);
void				ft_queuedelete(t_queue **queue_ptr);
int					ft_queuepush(t_queue *queue, t_list *node);
t_list				*ft_queuepop(t_queue *queue);

/*
** Main functions
*/

int					ft_vasprintf(char **ret, const char *format, va_list ap);
int					ft_asprintf(char **ret, const char *format, ...);
int					ft_printf(const char *format, ...);

/*
** Structures management
*/

t_vector			*ft_vector_create(int size);
void				ft_vector_add(t_vector *vector, void *ptr);
void				ft_vector_resize(t_vector *vector);
void				ft_vector_destroy(t_vector **vector);

t_u8_string			*ft_u8_string_new(char *str, int size);
void				ft_u8_string_del(t_u8_string **str);

t_conversion		*ft_printf_conversion_new(void);
void				ft_printf_conversion_del(t_conversion **conv);

t_result			*ft_printf_result_create(const char *format, va_list ap);
void				ft_printf_result_destroy(t_result **result);
char				*ft_printf_result_compose(t_result *result);

/*
** Parsing
*/

int					ft_printf_format_parse(t_result *result);
int					ft_printf_format_parse_string(t_result *result);
int					ft_printf_format_parse_conversion(t_result *result);

/*
** Conversion management
*/

void				ft_printf_format_conversion(t_u8_string *mbs,
												t_conversion *conv);
void				ft_printf_format_conversion_alternate(t_u8_string *mbs,
											t_conversion *conv);
void				ft_printf_format_conversion_sign(t_u8_string *mbs,
														t_conversion *conv);
void				ft_printf_format_conversion_padding(t_u8_string *mbs,
														t_conversion *conv);
void				ft_printf_format_conversion_precision(t_u8_string *mbs,
											t_conversion *conv);

int					ft_printf_conversion_compute(t_result *result);
int					ft_printf_conversion_dispatch(t_conversion *conv,
													char **str,
													t_result *result);
void				ft_printf_conversion_load(t_result *result);

int					ft_printf_conversion_get_arg_index(t_result *result);
int					ft_printf_conversion_get_arg(t_conversion *conv,
													t_result *result);
int					ft_printf_conversion_get_flags(t_conversion *conv,
													t_result *result);
int					ft_printf_conversion_get_width(t_conversion *conv,
													t_result *result);
int					ft_printf_conversion_get_precision(t_conversion *conv,
														t_result *result);
int					ft_printf_conversion_get_modifier(t_conversion *conv,
														t_result *result);
int					ft_printf_conversion_get_specifier(t_conversion *conv,
														t_result *result);

/*
** Conversion functions
*/

int					ft_printf_conversion_fn_s(t_result *result,
												t_conversion *conv,
												char **str);
int					ft_printf_conversion_fn_p(t_result *result,
												t_conversion *conv,
												char **str);
int					ft_printf_conversion_fn_d(t_result *result,
												t_conversion *conv,
												char **str);
int					ft_printf_conversion_fn_o(t_result *result,
												t_conversion *conv,
												char **str);
int					ft_printf_conversion_fn_u(t_result *result,
												t_conversion *conv,
												char **str);
int					ft_printf_conversion_fn_x(t_result *result,
												t_conversion *conv,
												char **str);
int					ft_printf_conversion_fn_c(t_result *result,
												t_conversion *conv,
												char **str);
int					ft_printf_conversion_fn_n(t_result *result,
												t_conversion *conv,
												char **str);
int					ft_printf_conversion_fn_percent(t_result *result,
													t_conversion *conv,
													char **str);

/*
** Error handling
*/

int					ft_printf_error(t_result **result, char *str,
									int error_code);

/*
** Helpers
*/

void				ft_printf_forward_args(va_list ap, int steps);
void				ft_printf_reset_args(t_result *result);
void				ft_printf_reset_forward_args(t_result *result, int steps);
void				ft_printf_get_width_precision(t_result *result,
													t_conversion *conv);
char				*ft_printf_get_padding(int size, char c);
int					ft_intlen(int nb);

#endif
