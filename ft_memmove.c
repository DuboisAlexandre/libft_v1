/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 18:00:54 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:23:04 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Copies the 'n' first bytes from the memory space pointed by 'src' in the
** one pointed by 'dst' on a non destructive manner.
*/

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	size_t			i;
	unsigned char	*swap;

	swap = (unsigned char*)ft_memalloc(len);
	i = 0;
	while (i < len)
	{
		swap[i] = ((unsigned char *)src)[i];
		i++;
	}
	i = 0;
	while (i < len)
	{
		((unsigned char *)dst)[i] = swap[i];
		i++;
	}
	ft_memdel((void**)&swap);
	return (dst);
}
