/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 14:08:35 by adubois           #+#    #+#             */
/*   Updated: 2015/12/13 13:17:40 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Load the next BUFF_SIZE bytes from the file and set the file status to 0 if
** the EOF has been reached.
*/

static int			gnl_read(t_gnl_file *file)
{
	int		result;

	if ((result = read(file->fd, file->buffer, GNL_BUFF_SIZE)) >= 0)
	{
		file->buffer[result] = '\0';
		file->start = 0;
	}
	if (result == -1)
		return (-1);
	file->status = (result >= 1) ? 1 : 0;
	return (result);
}

/*
** Create a new node for the file descriptor, initialize it and add it at the
** end of the linked list.
*/

static t_gnl_file	*gnl_file_push(t_gnl_file **list, int fd)
{
	t_gnl_file	*file;
	t_gnl_file	*index;

	if ((file = (t_gnl_file *)malloc(sizeof(t_gnl_file))) == NULL)
		return (NULL);
	file->fd = fd;
	file->start = 0;
	file->status = 1;
	file->buffer[0] = '\0';
	file->next = NULL;
	if (*list == NULL)
		*list = file;
	else
	{
		index = *list;
		while (index->next != NULL)
			index = index->next;
		index->next = file;
	}
	return (file);
}

/*
** Terminate the string to return appending the characters of the buffer
** from index file->start to the first occurence of a new line character.
*/

static int			gnl_get_line_ending(t_gnl_file *file, char **line)
{
	char			*eol;
	char			*str;
	char			*swap;

	if (file->status == 1 &&
		(eol = ft_strchr(file->buffer + file->start, '\n')) != NULL)
	{
		if ((str = ft_strsub(file->buffer, file->start,
							ft_abs(eol - file->buffer) - file->start)) == NULL)
			return (-1);
		if ((swap = ft_strjoin(*line, str)) == NULL)
			return (-1);
		free(*line);
		free(str);
		*line = swap;
		file->start = ft_abs(eol - file->buffer) + 1;
	}
	return (file->status);
}

/*
** Create and populate the string to return until there's new line
** character between the index file->start and the end of the buffer.
*/

static int			gnl_get_line(t_gnl_file *file, char **line)
{
	int		result;
	char	*eol;
	char	*swap;

	if ((*line = ft_strnew(0)) == NULL)
		return (-1);
	while (file->status == 1 &&
			(eol = ft_strchr(file->buffer + file->start, '\n')) == NULL)
	{
		swap = ft_strjoin(*line, file->buffer + file->start);
		free(*line);
		if (swap == NULL)
			return (-1);
		*line = swap;
		if ((result = gnl_read(file)) < 1)
			break ;
	}
	if (result == -1 || gnl_get_line_ending(file, line) == -1)
	{
		free(*line);
		return (-1);
	}
	return (file->status);
}

/*
** Return a new string containing the next line of the file pointed
** to by the file descriptor.
*/

int					ft_get_next_line(int const fd, char **line)
{
	static t_gnl_file	*list = NULL;
	t_gnl_file			*file;

	file = list;
	while (file != NULL)
	{
		if (file->fd == fd)
			break ;
		file = file->next;
	}
	if (file == NULL)
	{
		if ((file = gnl_file_push(&list, fd)) == NULL)
			return (-1);
		else if (gnl_read(file) == -1)
			return (-1);
	}
	if (file->status == 1)
		return (gnl_get_line(file, line));
	else
		return (file->status);
}
