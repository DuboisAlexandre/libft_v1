/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_memory.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/30 16:54:29 by adubois           #+#    #+#             */
/*   Updated: 2016/01/14 18:27:50 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static void	ft_print_memory_hexa(unsigned char *chars, unsigned int size)
{
	unsigned int	i;
	char			*charset;

	charset = "0123456789abcdef";
	i = 0;
	while (i < size)
	{
		ft_putchar(charset[chars[i] / 16]);
		ft_putchar(charset[chars[i] % 16]);
		i++;
		if (i % 2 == 0)
			ft_putchar(' ');
	}
	while (i < 16)
	{
		ft_putstr("  ");
		i++;
		if (i % 2 == 0)
			ft_putchar(' ');
	}
}

static void	ft_print_memory_ascii(unsigned char *chars, unsigned int size)
{
	unsigned int	i;

	i = 0;
	while (i < size)
	{
		if (chars[i] < 32 || chars[i] > 126)
			ft_putchar('.');
		else
			ft_putchar(chars[i]);
		i++;
	}
	ft_putchar('\n');
}

static void	ft_print_memory_line(unsigned char *chars, unsigned int size)
{
	ft_print_memory_hexa(chars, size);
	ft_print_memory_ascii(chars, size);
}

void		ft_print_memory(const void *addr, unsigned int size)
{
	unsigned char	chars[16];
	unsigned int	i;

	i = 0;
	while (i < size)
	{
		chars[i % 16] = ((unsigned char*)addr)[i];
		i++;
		if (i % 16 == 0)
			ft_print_memory_line(chars, 16);
	}
	if (i % 16 != 0)
		ft_print_memory_line(chars, i % 16);
}
