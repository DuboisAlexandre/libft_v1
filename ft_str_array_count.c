/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_array_count.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 19:18:42 by adubois           #+#    #+#             */
/*   Updated: 2016/04/27 19:51:06 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_str_array_count(char **array)
{
	int		size;

	if (array == NULL)
		return (-1);
	size = 0;
	while (array[size] != NULL)
		size++;
	return (size);
}
