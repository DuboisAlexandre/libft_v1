/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcasechr.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 10:51:02 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:40:14 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns a pointer on the first occurence of the character 'c' in the string
** 's'. This function is case insensitive.
*/

char	*ft_strcasechr(const char *s, int c)
{
	size_t	i;
	size_t	str_len;
	char	needle;

	if (s == NULL)
		return (NULL);
	needle = ft_tolower((char)c);
	str_len = ft_strlen(s);
	i = 0;
	while (i <= str_len)
	{
		if (ft_tolower(s[i]) == needle)
			return ((char*)s + i);
		i++;
	}
	return (NULL);
}
