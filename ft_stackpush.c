/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stackpush.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 10:27:48 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:36:51 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Add the node 'node' on the stack 'stack'.
*/

int		ft_stackpush(t_stack *stack, t_list *node)
{
	if (stack == NULL || node == NULL)
		return (1);
	node->next = stack->top;
	stack->top = node;
	stack->size += 1;
	return (0);
}
