/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_error.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/29 22:04:40 by adubois           #+#    #+#             */
/*   Updated: 2016/04/27 16:03:14 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Cleans the main data structure, writes the error message received on
** stderr and returns the given error code, -1 by default.
*/

int		ft_printf_error(t_result **result, char *str, int error_code)
{
	ft_printf_result_destroy(result);
	write(2, str, ft_strlen(str));
	if (error_code >= 0)
		return (-1);
	return (error_code);
}
