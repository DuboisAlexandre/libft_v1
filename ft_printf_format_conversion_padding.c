/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_format_conversion_padding.c              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/01 21:48:05 by adubois           #+#    #+#             */
/*   Updated: 2016/04/27 17:03:45 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Padding function, for the string formating, and her subfunctons.
*/

static char		*ft_printf_padding_join(t_u8_string *mbs, char *padding,
										int invert)
{
	char	*tmp;
	int		len;

	len = ft_strlen(padding);
	tmp = ft_strnew(len + mbs->size);
	if (invert)
	{
		ft_memcpy(tmp, padding, len);
		ft_memcpy(tmp + len, mbs->str, mbs->size);
	}
	else
	{
		ft_memcpy(tmp, mbs->str, mbs->size);
		ft_memcpy(tmp + mbs->size, padding, len);
	}
	return (tmp);
}

static int		ft_printf_padding_size(t_u8_string *mbs, t_conversion *conv)
{
	int		size;

	size = conv->width - mbs->size;
	if (conv->flags & (1 << (7 - ft_strchrpos(FLAGS, '0'))))
	{
		if ((conv->specifier == 'x' &&
				conv->flags & (1 << (7 - ft_strchrpos(FLAGS, '#')))) ||
			conv->specifier == 'p')
			size -= 2;
		if ((conv->flags & (1 << (7 - ft_strchrpos(FLAGS, '+'))) ||
			conv->flags & (1 << (7 - ft_strchrpos(FLAGS, ' ')))) &&
			mbs->str[0] != '-')
			size--;
	}
	if (size < 0)
		size = 0;
	return (size);
}

void			ft_printf_format_conversion_padding(t_u8_string *mbs,
													t_conversion *conv)
{
	char	*padding;
	char	*tmp;
	int		size;

	if (conv->width <= mbs->size)
		return ;
	size = ft_printf_padding_size(mbs, conv);
	padding = ft_printf_get_padding(size, ' ');
	if (conv->flags & (1 << (7 - ft_strchrpos(FLAGS, '-'))))
		tmp = ft_printf_padding_join(mbs, padding, 0);
	else if ((conv->flags & (1 << (7 - ft_strchrpos(FLAGS, '0')))) &&
				(conv->precision == -2 || conv->specifier == 'c' ||
					conv->specifier == 's'))
	{
		tmp = ft_printf_padding_join(mbs, ft_memset(padding, '0', size), 1);
		tmp[0] = (tmp[size] == '-') ? '-' : tmp[0];
		tmp[size] = (tmp[size] == '-') ? '0' : tmp[size];
	}
	else
		tmp = ft_printf_padding_join(mbs, padding, 1);
	free(padding);
	free(mbs->str);
	mbs->str = tmp;
	mbs->size += size;
}
