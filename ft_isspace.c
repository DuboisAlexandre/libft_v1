/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isspace.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 17:06:17 by adubois           #+#    #+#             */
/*   Updated: 2016/04/27 16:26:24 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_isspace(int c)
{
	unsigned char	chr;

	chr = (unsigned char)c;
	if (chr == '\t' || chr == '\n' || chr == '\v' ||
		chr == '\f' || chr == '\r' || chr == ' ')
		return (1);
	else
		return (0);
}
