/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchrpos.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/29 15:06:52 by adubois           #+#    #+#             */
/*   Updated: 2016/04/29 15:14:42 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

int			ft_strrchrpos(const char *str, char chr)
{
	int		pos;

	pos = ft_strlen(str);
	while (pos >= 0)
	{
		if (str[pos] == chr)
			return (pos);
		--pos;
	}
	return (-1);
}
