/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_helpers.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/30 18:56:09 by adubois           #+#    #+#             */
/*   Updated: 2016/04/27 16:02:01 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Goes forward in the arguments list and stops at the given index.
*/

void		ft_printf_forward_args(va_list ap, int steps)
{
	while (--steps > 0)
		va_arg(ap, void*);
}

/*
** Resets the arguments list
*/

void		ft_printf_reset_args(t_result *result)
{
	va_end(result->ap);
	va_copy(result->ap, result->ap_bkp);
}

/*
** Resets the arguments list and goes forward to the given index.
*/

void		ft_printf_reset_forward_args(t_result *result, int steps)
{
	ft_printf_reset_args(result);
	ft_printf_forward_args(result->ap, steps);
}

/*
** Grabs the width and precision of the given conversion if the have
** to be taken from the arguments list.
*/

void		ft_printf_get_width_precision(t_result *result, t_conversion *conv)
{
	if (conv->width == -1)
	{
		if (conv->width_arg != 0)
			ft_printf_reset_forward_args(result, conv->width_arg);
		conv->width = va_arg(result->ap, int);
		if (conv->width < 0)
		{
			conv->width *= -1;
			conv->flags |= 1 << (7 - ft_strchrpos(FLAGS, '-'));
		}
	}
	if (conv->precision == -1)
	{
		if (conv->precision_arg != 0)
			ft_printf_reset_forward_args(result, conv->precision_arg);
		conv->precision = va_arg(result->ap, int);
	}
}

/*
** Creates and returns a new string for the padding.
*/

char		*ft_printf_get_padding(int size, char c)
{
	char	*padding;

	padding = ft_strnew(size);
	ft_memset(padding, c, size);
	return (padding);
}
