/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_queuepush.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 13:48:51 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:34:07 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Add the node 'node' in the queue 'queue'.
*/

int		ft_queuepush(t_queue *queue, t_list *node)
{
	if (queue == NULL || node == NULL)
		return (1);
	if (queue->top == NULL)
		queue->top = node;
	else
		queue->bottom->next = node;
	queue->bottom = node;
	queue->size += 1;
	return (0);
}
