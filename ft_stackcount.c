/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stackcount.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 10:26:12 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:52:30 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns the size of the stack 'stack'.
*/

int		ft_stackcount(t_stack *stack)
{
	if (stack == NULL)
		return (-1);
	return (ft_lstcount(stack->top));
}
