/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 21:24:56 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:28:57 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Prints the number 'n' on the output pointed by the file descriptor 'fd'.
*/

void	ft_putnbr_fd(int n, int fd)
{
	int		i;
	int		negative;
	char	str[12];

	ft_bzero(str, 12);
	negative = (n < 0) ? 1 : 0;
	i = 0;
	if (n == 0)
	{
		str[i] = '0';
		i++;
	}
	while (n != 0)
	{
		str[i] = ft_abs(n % 10) + '0';
		n /= 10;
		i++;
	}
	if (negative)
		str[i++] = '-';
	ft_putstr_fd(ft_strrev(str), fd);
}
