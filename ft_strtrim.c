/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 22:12:19 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:31:55 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns a copy of the character string 's' without leading or trailing
** whitespaces. The whitespace characters are ' ', '\n' ad '\t'.
*/

char	*ft_strtrim(char const *s)
{
	return (ft_strrtrim(ft_strltrim(s)));
}
