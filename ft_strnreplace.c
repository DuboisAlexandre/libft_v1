/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnreplace.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 12:58:36 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:18:04 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Replaces the occurences of the character string 'needle' in 'haystack' by
** 'replace', but not more than 'n' occurences.
*/

char	*ft_strnreplace(char *haystack, char *needle, char *replace, int n)
{
	int		occurences;
	char	*result;
	char	*next;

	if (haystack == NULL)
		return (NULL);
	if (needle == NULL)
		return (ft_strdup(haystack));
	if (replace == NULL)
		replace = "";
	occurences = ft_strstr_count(haystack, needle);
	occurences = (occurences > n) ? n : occurences;
	result = ft_strnew((ft_strlen(haystack) - (occurences * ft_strlen(needle)))
						+ (occurences * ft_strlen(replace)));
	while (occurences-- > 0)
	{
		next = ft_strstr(haystack, needle);
		ft_strncat(result, haystack, (int)(next - haystack));
		ft_strcat(result, replace);
		haystack = next + ft_strlen(needle);
	}
	ft_strncat(result, haystack, (int)(ft_strchr(haystack, '\0') - haystack));
	return (result);
}
