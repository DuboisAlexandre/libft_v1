/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnodestrcmp.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 17:25:10 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:07:04 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Compares contents of two linked list nodes received as parameters as if
** the were character strings.
*/

int		ft_lstnodestrcmp(const t_list *node1, const t_list *node2)
{
	return (strcmp((char*)node1->content, (char*)node2->content));
}
