/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 16:30:50 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 18:58:54 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Delete the linked list pointed by 'alst' using the function received as
** parameter to free the memory allocated to the nodes' content.
*/

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*elem;
	t_list	*next;

	if (alst == NULL || *alst == NULL || del == NULL)
		return ;
	elem = *alst;
	while (elem != NULL)
	{
		next = elem->next;
		ft_lstdelone(&elem, (*del));
		elem = next;
	}
	*alst = NULL;
}
