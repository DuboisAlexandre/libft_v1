/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_conversion.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/25 12:57:54 by adubois           #+#    #+#             */
/*   Updated: 2016/04/27 16:39:45 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Creates and initializes a new t_conversion data structure and returns a
** pointer to it.
*/

t_conversion	*ft_printf_conversion_new(void)
{
	t_conversion	*conv;

	if (!(conv = (t_conversion*)malloc(sizeof(t_conversion))))
		exit(-1);
	conv->flags = 0;
	conv->modifier = '\0';
	conv->arg_index = 0;
	conv->width = 0;
	conv->width_arg = 0;
	conv->precision = -2;
	conv->precision_arg = 0;
	conv->uppercase = 0;
	conv->specifier = '\0';
	return (conv);
}

/*
** Deletes the given t_conversion data structure.
*/

void			ft_printf_conversion_del(t_conversion **conv)
{
	free(*conv);
	*conv = NULL;
}

/*
** Manages the computing of all the conversions, picking them one by one
** and storing the result in the strings array.
*/

int				ft_printf_conversion_compute(t_result *result)
{
	int				i;
	int				size;
	char			*str;
	t_conversion	*conv;
	t_u8_string		*u8_str;

	i = 0;
	while (result->convs->cells[i])
	{
		conv = (t_conversion*)result->convs->cells[i];
		u8_str = (t_u8_string*)result->strings->cells[i << 1];
		result->size += u8_str->size;
		if (-1 == (size = ft_printf_conversion_dispatch(conv, &str, result)))
			return (-1);
		u8_str = ft_u8_string_new(str, size);
		ft_printf_format_conversion(u8_str, conv);
		result->strings->cells[(i << 1) + 1] = u8_str;
		result->size += u8_str->size;
		if (result->manual_args)
			ft_printf_reset_args(result);
		++i;
	}
	u8_str = (t_u8_string*)result->strings->cells[result->strings->index - 2];
	result->size += u8_str->size;
	return (0);
}

/*
** Redirects the given conversion to the right function.
*/

int				ft_printf_conversion_dispatch(t_conversion *conv, char **str,
									t_result *result)
{
	char	*specifiers;
	int		pos;

	specifiers = SPECIFIERS;
	if (-1 == (pos = ft_strchrpos(specifiers, conv->specifier)))
		pos = ft_strchrpos(specifiers, 'c');
	ft_printf_get_width_precision(result, conv);
	if (conv->arg_index > 0)
		ft_printf_reset_forward_args(result, conv->arg_index);
	return (result->fn[pos](result, conv, str));
}

/*
** Loads all the conversion functions.
*/

void			ft_printf_conversion_load(t_result *result)
{
	result->fn[0] = ft_printf_conversion_fn_s;
	result->fn[1] = ft_printf_conversion_fn_p;
	result->fn[2] = ft_printf_conversion_fn_d;
	result->fn[3] = ft_printf_conversion_fn_d;
	result->fn[4] = ft_printf_conversion_fn_o;
	result->fn[5] = ft_printf_conversion_fn_u;
	result->fn[6] = ft_printf_conversion_fn_x;
	result->fn[7] = ft_printf_conversion_fn_c;
	result->fn[8] = ft_printf_conversion_fn_n;
	result->fn[9] = ft_printf_conversion_fn_percent;
}
