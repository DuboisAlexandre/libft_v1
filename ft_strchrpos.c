/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchrpos.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/25 11:06:47 by adubois           #+#    #+#             */
/*   Updated: 2016/04/27 16:40:56 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_strchrpos(const char *str, char chr)
{
	unsigned int	pos;

	pos = 0;
	while (*str)
	{
		if (*str == chr)
			return (pos);
		++pos;
		++str;
	}
	return (-1);
}
