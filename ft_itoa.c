/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 23:40:02 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:36:53 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns a pointer on a new string containing the ASCI representation of
** the integer received as parameter.
*/

char	*ft_itoa(int n)
{
	int		i;
	int		negative;
	char	*str;

	if ((str = ft_strnew(11)) == NULL)
		return (NULL);
	negative = (n < 0) ? 1 : 0;
	i = 0;
	if (n == 0)
	{
		str[i] = '0';
		i++;
	}
	while (n != 0)
	{
		str[i] = ft_abs(n % 10) + '0';
		n /= 10;
		i++;
	}
	if (negative)
		str[i++] = '-';
	return (ft_strrev(str));
}
