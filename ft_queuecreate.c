/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_queuecreate.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 13:38:25 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:32:23 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns a pointer on a new queue.
*/

t_queue		*ft_queuecreate(void)
{
	t_queue	*queue;

	if ((queue = (t_queue*)ft_memalloc(sizeof(t_queue))) == NULL)
		return (NULL);
	queue->top = NULL;
	queue->bottom = NULL;
	queue->size = 0;
	return (queue);
}
