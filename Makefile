#* ************************************************************************** *#
#*                                                                            *#
#*                                                        :::      ::::::::   *#
#*   Makefile                                           :+:      :+:    :+:   *#
#*                                                    +:+ +:+         +:+     *#
#*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        *#
#*                                                +#+#+#+#+#+   +#+           *#
#*   Created: 2015/11/26 18:04:58 by adubois           #+#    #+#             *#
#*   Updated: 2016/04/29 19:39:34 by adubois          ###   ########.fr       *#
#*                                                                            *#
#* ************************************************************************** *#

NAME = libft.a
CC = clang
CFLAGS = -Wall -Werror -Wextra
INC_PATH = -I .
SRC_PATH = .
OBJ_PATH = ./obj

SRC = \
ft_abs.c \
ft_atoi.c \
ft_bzero.c \
ft_get_next_line.c \
ft_intlen.c \
ft_isalnum.c \
ft_isalpha.c \
ft_isascii.c \
ft_isdigit.c \
ft_islower.c \
ft_isprint.c \
ft_isspace.c \
ft_isupper.c \
ft_itoa.c \
ft_itoa_base.c \
ft_lstadd.c \
ft_lstcount.c \
ft_lstdel.c \
ft_lstdelone.c \
ft_lstiter.c \
ft_lstmap.c \
ft_lstnew.c \
ft_lstnodefree.c \
ft_lstnodestrcmp.c \
ft_lstprintstr.c \
ft_lstpushback.c \
ft_lstpushfront.c \
ft_lstsort.c \
ft_max.c \
ft_memalloc.c \
ft_memccpy.c \
ft_memchr.c \
ft_memcmp.c \
ft_memcpy.c \
ft_memdel.c \
ft_memmove.c \
ft_memset.c \
ft_min.c \
ft_print_memory.c \
ft_printf.c \
ft_printf_conversion.c \
ft_printf_conversion_functions.c \
ft_printf_conversion_functions_integers.c \
ft_printf_conversion_get.c \
ft_printf_conversion_get_args.c \
ft_printf_error.c \
ft_printf_format.c \
ft_printf_format_conversion.c \
ft_printf_format_conversion_padding.c \
ft_printf_helpers.c \
ft_printf_result.c \
ft_putchar.c \
ft_putchar_fd.c \
ft_putendl.c \
ft_putendl_fd.c \
ft_putnbr.c \
ft_putnbr_fd.c \
ft_putstr.c \
ft_putstr_fd.c \
ft_queuecount.c \
ft_queuecreate.c \
ft_queuedelete.c \
ft_queuepop.c \
ft_queuepush.c \
ft_stackcount.c \
ft_stackcreate.c \
ft_stackdelete.c \
ft_stackpop.c \
ft_stackpush.c \
ft_strarraysort.c \
ft_strarraycount.c \
ft_strcasechr.c \
ft_strcasechr_count.c \
ft_strcasestr.c \
ft_strcasestr_count.c \
ft_strcat.c \
ft_strchr.c \
ft_strchr_count.c \
ft_strchrpos.c \
ft_strclr.c \
ft_strcmp.c \
ft_strcpy.c \
ft_strdel.c \
ft_strdup.c \
ft_strequ.c \
ft_striter.c \
ft_striteri.c \
ft_strjoin.c \
ft_strlcat.c \
ft_strlen.c \
ft_strltrim.c \
ft_strmap.c \
ft_strmapi.c \
ft_strncat.c \
ft_strncmp.c \
ft_strncpy.c \
ft_strnequ.c \
ft_strnew.c \
ft_strnlen.c \
ft_strnreplace.c \
ft_strnstr.c \
ft_strrchr.c \
ft_strrchrpos.c \
ft_strreplace.c \
ft_strrev.c \
ft_strrtrim.c \
ft_strsplit.c \
ft_strsplittolst.c \
ft_strstr.c \
ft_strstr_count.c \
ft_strsub.c \
ft_strtolower.c \
ft_strtoupper.c \
ft_strtrim.c \
ft_tolower.c \
ft_toupper.c \
ft_u8_string.c \
ft_utoa_base.c \
ft_vector.c \
ft_wcslen.c \
ft_wcstombs.c \
ft_wctomb.c

OBJ = $(SRC:%.c=$(OBJ_PATH)/%.o)

all: $(NAME)

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@mkdir -p $(OBJ_PATH)
	$(CC) -o $@ -c $< $(CFLAGS) $(INC_PATH)

clean:
	rm -rf $(OBJ_PATH)

fclean: clean
	rm -rf $(NAME)

re: fclean all

.PHONY: all clean fclean re
