/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/23 12:00:01 by adubois           #+#    #+#             */
/*   Updated: 2016/04/29 15:37:45 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Main function of the printf suite. All the others functions are wrappers
** of this one.
*/

int			ft_vasprintf(char **ret, const char *format, va_list ap)
{
	t_result	*result;
	int			to_return;

	result = ft_printf_result_create(format, ap);
	if (-1 == ft_printf_format_parse(result))
		return (ft_printf_error(&result, ERROR_FORMAT, 0));
	ft_printf_conversion_load(result);
	if (-1 == ft_printf_conversion_compute(result))
		return (ft_printf_error(&result, ERROR_CONVERSION, 0));
	*ret = ft_printf_result_compose(result);
	to_return = result->size;
	ft_printf_result_destroy(&result);
	return (to_return);
}

/*
** Wrapper of the main function that prints immediatly the result string.
*/

int			ft_printf(const char *format, ...)
{
	char	*result;
	va_list	ap;
	int		size;

	result = NULL;
	va_start(ap, format);
	size = ft_vasprintf(&result, format, ap);
	va_end(ap);
	if (result)
	{
		write(1, result, size);
		free(result);
	}
	return (size);
}

/*
** Wrapper of the main function that just handle the va_list.
*/

int			ft_asprintf(char **ret, const char *format, ...)
{
	va_list	ap;
	int		size;

	va_start(ap, format);
	size = ft_vasprintf(ret, format, ap);
	(*ret)[size] = '\0';
	va_end(ap);
	return (size);
}
