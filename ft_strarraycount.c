/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strarraycount.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 19:19:36 by adubois           #+#    #+#             */
/*   Updated: 2016/04/27 19:51:02 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Return the number of elements in an array of strings, -1 if an error occurs.
*/

int			ft_strarraycount(char **array)
{
	int		size;

	if (array == NULL)
		return (-1);
	size = 0;
	while (array[size] != NULL)
		size++;
	return (size);
}
