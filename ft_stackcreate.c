/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stackcreate.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 10:22:14 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:35:04 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns a pointer on a new stack.
*/

t_stack		*ft_stackcreate(void)
{
	t_stack	*stack;

	if ((stack = (t_stack*)ft_memalloc(sizeof(t_stack))) == NULL)
		return (NULL);
	stack->top = NULL;
	stack->size = 0;
	return (stack);
}
